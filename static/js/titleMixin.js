function getTitle (vm) {
    const { title } = vm.$options
    if (title) {
      return typeof title === 'function'
        ? title.call(vm)
        : title
    }
}
export default {
    created () {
        const title = getTitle(this)
        console.log(title)
        if (title) {
            document.title = '白金科技股份有限公司-' + title
        } else {
            document.title = '白金科技股份有限公司'
        }
    }
}
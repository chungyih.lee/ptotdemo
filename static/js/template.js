$(document).ready(function() {
    animate();
    rwd_fun();
    $(window).resize(rwd_fun);
});

function animate() {
    $(".inline").colorbox({ inline: true, width: "auto" }); //html���拳
    $("#footer").prepend($("#menu2"));
    $("#footer_c").after($("#social_fa_icon"));
    $("#footer_c").after($(".counter"));
    $(".cC").before($(".titleA"));
    $("#photo_explain").before($("#photo_group"));
    $(".p_img,.edit_c img").addClass("aniview");
    $("#social_fa_icon").addClass("aniview");
    $(".p_img").attr("av-animation", "slideInUp");
    $(".edit_c img").attr("av-animation", "fadeInLeft");
    $("#social_fa_icon").attr("av-animation", "fadeInDown");


    $(".s631_index_02_wc,.s631_index_03_wc,.s631_index_01_w").addClass("aniview");
    $(".s631_index_02_wc,.s631_index_03_wc,.s631_index_01_w").attr("av-animation", "fadeInDown");

    var options = {
        animateThreshold: 100,
        scrollPollInterval:20
    }
    $('.aniview').AniView(options);

    if ($("#wrap").find("#search").size() > 0) {
        $(".search_icon").css("display", "block");
    }
    if ($("#wrap").find("#page_left").size() > 0) {
        $("#page_left").before($(".titleA"));
    }
    if ($("#wrap").find(".p_photo").size() > 0) {
        $("#photo_explain").after($(".ss_icon"));
    }

}

function rwd_fun() {
    var width = window.innerWidth;
    var height = window.innerHeight;
    if (width > 1024) {

        $('#page_top').before($('#topnav_wrap'));
        $('#topnav').before($('.search_icon'));

    } else {
        $('#page_top').after($('#topnav_wrap'));
        $('#rwd_nav').after($('.search_icon'));

    }

}



$(document).ready(function() {

    $(window).scroll(function() {
        var scrollVal = $(this).scrollTop();
        if (scrollVal >= 120) {
            $("#m_nav").addClass("menu_top1");
        }
        if (scrollVal >= 150) {
            $("#m_nav").addClass("menu_top2");
        } else {
            $("#m_nav").removeClass("menu_top1 menu_top2");
        }
    });

});
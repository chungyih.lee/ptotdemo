
function addBookmarkForBrowser(sTitle, sUrl) {

    if (window.sidebar && window.sidebar.addPanel) {
        addBookmarkForBrowser = function (sTitle, sUrl) {
            window.sidebar.addPanel(sTitle, sUrl, "");
        }
    } else if (window.external) {
        addBookmarkForBrowser = function (sTitle, sUrl) {
            window.external.AddFavorite(sUrl, sTitle);
        }
    } else {
        addBookmarkForBrowser = function () {
            alert("do it yourself");
        }
    }

    return addBookmarkForBrowser(sTitle, sUrl);
}